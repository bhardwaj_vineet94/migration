<?php

namespace Drupal\taxexpertise\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;


/**
 * Source plugin for the expertise.
 *
 * @MigrateSource(
 *   id = "expertise"
 * )
 */
class Expertise extends SqlBase
{

    /**
     * {@inheritdoc}
     */
    public function query()
    {
        $query = $this->select('taxonomy_term_data', 'td')
            ->fields('td', ['name'])
            ->distinct()
            ->condition('td.name', '', '!=')
            ->condition('td.vid', '8', '=');
        return $query;
    }

    /**
     * {@inheritdoc}
     */
    public function fields()
    {
        $fields = [
            'name' => $this->t('name'),
        ];
        return $fields;
    }

    /**
     * {@inheritdoc}
     */
    public function getIds()
    {
        return [
            'name' => [
                'type' => 'string',
                'alias' => 'k',
            ],
        ];
    }

    // /**
    //  * {@inheritdoc}
    //  */
    public function prepareRow(Row $row)
    {

    }
}